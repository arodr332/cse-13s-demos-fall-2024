#include <stdio.h>

double exponent(double base, unsigned int power){
	// Recursive style
	
	if(power == 0){
	  return 1.0;
	} else {
	  return base * exponent(base, power - 1);
	}
}

int main(void){
   double results = exponent(2.0, 32);

   printf("\" four billion\" is: %f\n", results);

   return 0;
}
