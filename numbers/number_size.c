#include <stdio.h>

int main(void){
   printf("An int is this big: %lu\n", sizeof(int));
   printf("An long is this big %lu\n",sizeof(long));

   printf("An unsigned long is this big: %lu\n",sizeof(unsigned long));
   
   printf("An short  is this big: %lu\n",sizeof(short));
}
