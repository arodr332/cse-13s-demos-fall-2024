#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include "sqrt_lib.h"

double new_sqrt(double x){
  double guess = x / 4;

  while(true){
   // when do we stop?
   // if we can stop, then break;

   if(fabs((guess * guess) - x) < 0.00001) {
          printf("Done!!\n");
	  break;
   }
   // update the guess
   // Guess is the average of the current guess and (guess / x)
   guess = (guess + (x / guess)) / 2;
   printf("New guess is: %f\n", guess);
  }
  return guess;
}
