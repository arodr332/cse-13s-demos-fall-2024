#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include "sqrt_lib.h"

int main(void){

	double two = 2;
	double result = new_sqrt(two);
	printf("result is %f\n", result);

	result = new_sqrt(25.0);
	printf("result is %f\n", result);


    return 0;
}
