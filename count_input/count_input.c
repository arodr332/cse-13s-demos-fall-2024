#include <stdio.h>

int main(void){
    // EOF = Code for end of input / file
    // We are doing s
    
    int counter = 0;
    while(1){
	int c =  getchar();

	if (c == EOF){
	   break;
	}

	if (c == 's'){
	   counter++;
	}
    }

	printf("I found this many of the letter 's': %d\n ", counter);
}	
